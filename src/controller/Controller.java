package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller 
{

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag<Integer> createBag(ArrayList<Integer> values)
	{
         return new NumbersBag<Integer>(values);		
	}
	
	
	public static double getMean(NumbersBag<Double> bag)
	{
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag<Integer> bag)
	{
		return model.getMax(bag);
	}
	
}
